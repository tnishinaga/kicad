
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x03, 0xd9, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0xad, 0x95, 0x6d, 0x4c, 0x53,
 0x57, 0x18, 0xc7, 0xaf, 0xb0, 0x48, 0xa2, 0x4d, 0x5c, 0x50, 0xd0, 0xc0, 0xfc, 0x62, 0x4c, 0xad,
 0xd3, 0x99, 0x2c, 0xdb, 0x22, 0x42, 0x75, 0x19, 0x0a, 0xd4, 0x18, 0x6b, 0x30, 0xc4, 0x44, 0x93,
 0x41, 0x50, 0xea, 0xcb, 0x32, 0x36, 0x14, 0xe8, 0x7c, 0x09, 0x90, 0xed, 0x03, 0x1f, 0x94, 0x16,
 0x63, 0x0a, 0x59, 0x9c, 0x4a, 0xdc, 0x62, 0xa6, 0x45, 0xa5, 0x6a, 0x88, 0x21, 0x2e, 0x2a, 0x6e,
 0x85, 0x8d, 0x5b, 0x0c, 0xa1, 0x0a, 0x03, 0xc1, 0x8a, 0x3a, 0x44, 0xb0, 0x62, 0x5f, 0x6e, 0x4b,
 0xb1, 0xe5, 0xef, 0x39, 0x07, 0x7b, 0xd7, 0x17, 0xac, 0xf5, 0xe5, 0x24, 0xff, 0x9c, 0x7b, 0x9e,
 0x73, 0xee, 0xf3, 0x3b, 0xe7, 0x79, 0x9e, 0x73, 0x2f, 0xc7, 0xd1, 0x56, 0xc0, 0x6d, 0x20, 0x32,
 0xc5, 0xef, 0x8e, 0x1f, 0x57, 0xd4, 0x2a, 0x40, 0x45, 0x9e, 0x41, 0x6c, 0x4c, 0x7e, 0x1b, 0x7d,
 0x9e, 0x5b, 0x32, 0x57, 0x1c, 0x07, 0x68, 0x7c, 0x6d, 0xed, 0x5a, 0x13, 0xe9, 0x95, 0x5c, 0x58,
 0x53, 0x71, 0x4a, 0xbf, 0x23, 0xea, 0xf4, 0x2d, 0x01, 0xff, 0xab, 0x46, 0xb1, 0x31, 0x18, 0x40,
 0x76, 0xfe, 0x9e, 0x01, 0x7c, 0x28, 0xc0, 0xf3, 0x26, 0x80, 0x59, 0xdf, 0xcf, 0x42, 0x56, 0x4d,
 0x56, 0x24, 0x88, 0x27, 0x14, 0x80, 0x68, 0x01, 0xd3, 0x77, 0x4e, 0x67, 0xe3, 0xf9, 0x7b, 0xe7,
 0x8b, 0xb6, 0x8c, 0x9a, 0x0c, 0x36, 0x0e, 0xf4, 0x13, 0x15, 0x20, 0xf9, 0x87, 0x64, 0xd1, 0x2e,
 0xd7, 0xc8, 0x99, 0x8d, 0xf6, 0x7e, 0x08, 0x9d, 0x97, 0x56, 0x48, 0x21, 0x29, 0x94, 0xb0, 0x71,
 0x9c, 0x6a, 0x1a, 0x62, 0x0b, 0xa6, 0x45, 0x06, 0x48, 0xbe, 0x95, 0x20, 0xb3, 0x26, 0x93, 0x39,
 0xa3, 0x7d, 0x42, 0x71, 0x02, 0xb3, 0x53, 0xa7, 0x69, 0xda, 0x34, 0x66, 0x5f, 0xa9, 0x5d, 0x89,
 0xb8, 0x5d, 0x71, 0x41, 0x3b, 0x2e, 0x2d, 0xff, 0x08, 0x6d, 0x55, 0x32, 0xb4, 0x56, 0x2d, 0xc6,
 0xaa, 0x22, 0xc9, 0xab, 0x01, 0x54, 0x73, 0xf6, 0xcc, 0x41, 0xa6, 0xee, 0x25, 0x84, 0xf4, 0x89,
 0xc5, 0x89, 0x22, 0xc4, 0x7f, 0x92, 0x40, 0x6d, 0xd1, 0xad, 0x06, 0x7f, 0xe4, 0x33, 0x78, 0xc7,
 0xec, 0x70, 0x3e, 0x34, 0xe1, 0x8f, 0xaa, 0x25, 0x91, 0x01, 0xaf, 0x83, 0xf8, 0x4f, 0xe2, 0xd7,
 0x66, 0x02, 0x30, 0x31, 0x80, 0x0d, 0x8e, 0x07, 0x3c, 0xae, 0x1c, 0x8a, 0x02, 0x40, 0x45, 0xc3,
 0x13, 0x18, 0x2e, 0x1a, 0xbe, 0xd0, 0x35, 0xd2, 0x03, 0x52, 0xa8, 0x4e, 0xaa, 0xa0, 0x37, 0xa8,
 0xd1, 0x77, 0xa1, 0x10, 0xad, 0xd5, 0xcb, 0x20, 0x2f, 0x9a, 0x39, 0x35, 0x60, 0xde, 0xce, 0x0f,
 0xb0, 0xb9, 0xf4, 0x43, 0xa4, 0x7c, 0x37, 0x43, 0x74, 0x40, 0x13, 0x19, 0x5a, 0x55, 0x49, 0xea,
 0x24, 0x2c, 0x38, 0xb0, 0x00, 0xb2, 0x0a, 0x19, 0x46, 0x47, 0x87, 0xd1, 0x7f, 0xee, 0x02, 0xee,
 0xd4, 0x37, 0xe0, 0xb1, 0xd9, 0x0c, 0xb5, 0xbe, 0x24, 0x3c, 0xc9, 0xa9, 0x9a, 0x54, 0x28, 0x34,
 0x29, 0x68, 0xd5, 0x7e, 0x82, 0xbe, 0xcb, 0x7b, 0xc1, 0xd7, 0xca, 0xb1, 0x45, 0x1d, 0x3f, 0x65,
 0xd9, 0xc6, 0x6e, 0x8f, 0x45, 0xf6, 0x2f, 0xd9, 0x6c, 0xbc, 0xf5, 0xd4, 0x56, 0x3c, 0x7b, 0xf4,
 0x1f, 0xae, 0x49, 0xbf, 0x62, 0xea, 0x2a, 0xa9, 0x44, 0x83, 0xf1, 0x34, 0x7b, 0x8f, 0x34, 0x09,
 0x51, 0x22, 0x03, 0xd0, 0xc5, 0xea, 0x23, 0x69, 0xe8, 0x3a, 0xab, 0xa2, 0x13, 0xb0, 0x0f, 0xb4,
 0x40, 0x7f, 0x70, 0xc9, 0x94, 0x80, 0x2b, 0xed, 0x8d, 0xf0, 0xb8, 0x05, 0x98, 0xfb, 0x4d, 0x18,
 0x1c, 0xba, 0x07, 0xcb, 0x19, 0x83, 0x08, 0xf8, 0x27, 0x2b, 0x17, 0xc2, 0x53, 0x2b, 0x6e, 0xde,
 0xf9, 0x1b, 0xbe, 0x31, 0x8f, 0xe0, 0x15, 0x5c, 0x0e, 0xe2, 0xee, 0x4b, 0x06, 0xf8, 0x5a, 0x97,
 0x0e, 0xfe, 0xf0, 0xa7, 0xb0, 0xde, 0x36, 0xe0, 0xb6, 0x3e, 0x1f, 0xfb, 0x48, 0xd9, 0x85, 0x02,
 0xf2, 0xea, 0xf2, 0x30, 0xdc, 0xdb, 0x83, 0xe6, 0x65, 0x0a, 0x74, 0x95, 0x56, 0xa2, 0x73, 0xc7,
 0x7e, 0xd1, 0xb9, 0x5f, 0x7f, 0x7e, 0xae, 0x44, 0x4f, 0xb9, 0x16, 0x2d, 0xf2, 0x1c, 0x98, 0xbf,
 0x29, 0x83, 0xd7, 0x29, 0x58, 0xb8, 0x18, 0x55, 0x0c, 0xa8, 0x68, 0x72, 0x6a, 0x2b, 0xa5, 0x28,
 0xdc, 0x3f, 0x8f, 0x5d, 0x9a, 0xd0, 0x1c, 0xf4, 0x0f, 0x74, 0xa3, 0x23, 0xaf, 0x38, 0xcc, 0xe9,
 0x5f, 0x5f, 0x28, 0x61, 0x4c, 0xdd, 0x88, 0x6b, 0x8b, 0xd2, 0xc3, 0xe6, 0x6c, 0x1d, 0x5d, 0xf6,
 0xa8, 0xab, 0xe8, 0x56, 0xdf, 0x4d, 0xf4, 0xd7, 0x9d, 0x46, 0xf3, 0xd2, 0x4c, 0xd1, 0x41, 0xf7,
 0x4f, 0xd5, 0x70, 0x3b, 0x1d, 0xb0, 0x8d, 0x8e, 0xe0, 0x49, 0xf7, 0xbf, 0x30, 0xae, 0xc8, 0x16,
 0xe7, 0xe8, 0x66, 0xbc, 0x36, 0xa7, 0x33, 0xea, 0x7b, 0x30, 0xbb, 0x68, 0x36, 0x4c, 0x24, 0xbe,
 0x96, 0x9f, 0x7f, 0x63, 0x0e, 0xae, 0x7f, 0x9c, 0x81, 0x31, 0xc1, 0x89, 0xdc, 0x5f, 0x73, 0xd9,
 0xba, 0x4b, 0x1d, 0x06, 0x58, 0xea, 0xce, 0x4c, 0xce, 0x2d, 0x5e, 0x03, 0xf8, 0x26, 0x7c, 0x24,
 0x07, 0xe9, 0x41, 0x5f, 0xd3, 0x48, 0x97, 0x6c, 0x61, 0xd9, 0x42, 0x5c, 0x34, 0x5f, 0xc4, 0xbd,
 0x63, 0xbf, 0x4f, 0xee, 0x52, 0x96, 0x4e, 0x12, 0xee, 0x82, 0xf2, 0xa8, 0x92, 0xad, 0xd5, 0x5c,
 0xd5, 0xe0, 0x7e, 0x63, 0xd3, 0x4b, 0xc0, 0x6a, 0x60, 0x62, 0x62, 0x82, 0x00, 0xa4, 0x41, 0xff,
 0x83, 0x57, 0x7d, 0x8b, 0xa8, 0x73, 0x03, 0x5f, 0x8f, 0x51, 0x8b, 0x05, 0x6d, 0xeb, 0xf2, 0xc5,
 0x30, 0x0c, 0x9c, 0x6f, 0x44, 0x5b, 0xaf, 0x11, 0xba, 0x1b, 0x3a, 0x58, 0x9f, 0x3c, 0x42, 0xe7,
 0xf6, 0x7d, 0xe2, 0x5c, 0xef, 0x8f, 0x87, 0x7d, 0x5e, 0x97, 0x9b, 0x84, 0x68, 0x1b, 0xb7, 0x3e,
 0xd2, 0xd7, 0x34, 0x59, 0x3d, 0x99, 0x68, 0xab, 0x75, 0x08, 0xbc, 0xb2, 0x20, 0x28, 0x89, 0xb4,
 0xa2, 0xee, 0x56, 0x1f, 0xc5, 0xa0, 0xbe, 0x01, 0x1d, 0xf9, 0x25, 0x61, 0x49, 0x1e, 0x6e, 0x6a,
 0x76, 0x05, 0xfe, 0x36, 0xf9, 0xa9, 0xfe, 0xc9, 0x74, 0xf7, 0x74, 0x7c, 0xc2, 0x78, 0x1c, 0x83,
 0x37, 0x8c, 0xec, 0x45, 0x63, 0x5a, 0x0e, 0x2b, 0xc5, 0x50, 0x87, 0xb4, 0x92, 0xda, 0x73, 0x76,
 0xb1, 0xfc, 0xf0, 0x1b, 0x54, 0xf0, 0x0a, 0x6e, 0x1b, 0x17, 0x6d, 0x23, 0xf1, 0x8c, 0xf3, 0xb9,
 0xdc, 0x0f, 0x9f, 0xb6, 0xb4, 0x3b, 0xc8, 0x45, 0x72, 0x91, 0xe3, 0x0b, 0x3d, 0xe5, 0xd5, 0xe3,
 0x62, 0xb9, 0xa6, 0x64, 0xc3, 0xd6, 0xd9, 0xed, 0x20, 0x6b, 0xac, 0xcf, 0x9f, 0x39, 0x04, 0xcf,
 0xd0, 0x88, 0xc3, 0x37, 0xee, 0xdb, 0xc1, 0xbd, 0x49, 0x23, 0x90, 0x24, 0xa2, 0x4d, 0x44, 0xf1,
 0x44, 0xcb, 0xc7, 0x86, 0x46, 0xec, 0x7e, 0xc0, 0xad, 0xc2, 0x0a, 0x3c, 0xb7, 0x3b, 0x5a, 0x89,
 0x3d, 0x86, 0x48, 0x46, 0xb4, 0x8a, 0x7b, 0x97, 0x46, 0x1c, 0xcc, 0x20, 0x27, 0x71, 0xf7, 0x94,
 0x69, 0x40, 0xf5, 0xb8, 0xf1, 0xaa, 0xd7, 0xe7, 0x72, 0x69, 0x43, 0xd7, 0xbd, 0x00, 0xea, 0x08,
 0x36, 0xee, 0xa8, 0xea, 0x10, 0xda, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42,
 0x60, 0x82,
};

const BITMAP_OPAQUE options_board_xpm[1] = {{ png, sizeof( png ), "options_board_xpm" }};

//EOF
