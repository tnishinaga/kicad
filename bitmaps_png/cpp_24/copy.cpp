
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x00, 0xfd, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0x63, 0xf8, 0xff, 0xff, 0x3f,
 0xe3, 0xae, 0x5d, 0xbb, 0xf2, 0x77, 0xee, 0xdc, 0x79, 0x17, 0x88, 0xff, 0x93, 0x81, 0x7f, 0x00,
 0xf1, 0x91, 0x1d, 0x3b, 0x76, 0x98, 0x31, 0x60, 0x03, 0x40, 0x89, 0x02, 0x32, 0x0d, 0x46, 0xc7,
 0x5f, 0x80, 0x66, 0xa9, 0x60, 0x58, 0x00, 0x94, 0xb8, 0x47, 0x25, 0x0b, 0x40, 0xb8, 0x05, 0x9b,
 0x05, 0xff, 0xa8, 0x68, 0xc1, 0x2a, 0x6c, 0x16, 0x60, 0x28, 0xdc, 0xb8, 0x6d, 0xe7, 0xff, 0xbc,
 0xf9, 0xfb, 0xfe, 0x07, 0x4d, 0x3d, 0xf8, 0xdf, 0x7f, 0x0a, 0xf1, 0xd8, 0x69, 0xc2, 0xa1, 0xc7,
 0x52, 0x8d, 0x47, 0x57, 0x49, 0x35, 0x1c, 0xee, 0x14, 0x6f, 0x38, 0xac, 0x84, 0xd5, 0x02, 0x90,
 0xe1, 0x26, 0x5d, 0x87, 0xfe, 0x4b, 0x35, 0x1e, 0xa1, 0x14, 0x7f, 0x96, 0x6c, 0x3a, 0x6c, 0x8c,
 0x61, 0x41, 0xce, 0xbc, 0x7d, 0xd4, 0x30, 0x1c, 0x82, 0x9b, 0x8e, 0xec, 0xc5, 0xb0, 0xc0, 0x77,
 0xf2, 0x41, 0xea, 0x59, 0xd0, 0x78, 0xe4, 0x13, 0x86, 0x05, 0x1e, 0x13, 0xa9, 0x6a, 0xc1, 0xef,
 0x51, 0x0b, 0x46, 0x2d, 0x18, 0x22, 0x16, 0xfc, 0xa4, 0xb5, 0x05, 0xc7, 0x68, 0x6a, 0xc1, 0xee,
 0xdd, 0xbb, 0x2d, 0x40, 0x95, 0x05, 0xcd, 0x2c, 0x80, 0xd6, 0x6a, 0x2a, 0x40, 0xc3, 0xdb, 0x41,
 0xe5, 0xb9, 0x4d, 0xcf, 0xc1, 0xe7, 0x54, 0xb7, 0x00, 0x19, 0x00, 0x05, 0xb7, 0x50, 0xd1, 0x82,
 0x9f, 0x18, 0x16, 0x48, 0x36, 0x1e, 0xe9, 0xa3, 0xa2, 0x05, 0x17, 0x31, 0x7d, 0xd0, 0x7c, 0x44,
 0x1d, 0x28, 0xf1, 0x8d, 0x1a, 0x16, 0x48, 0x37, 0x1d, 0x4d, 0xc0, 0xda, 0xd2, 0x90, 0x6e, 0x38,
 0x62, 0x01, 0x54, 0x70, 0x90, 0x4c, 0x8b, 0xfe, 0x00, 0xf1, 0x15, 0x60, 0xd5, 0x99, 0x08, 0x32,
 0x0b, 0x00, 0x3e, 0xd4, 0x68, 0x82, 0x19, 0x33, 0x90, 0xf1, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45,
 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE copy_xpm[1] = {{ png, sizeof( png ), "copy_xpm" }};

//EOF
