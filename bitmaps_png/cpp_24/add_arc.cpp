
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x04, 0x00, 0x00, 0x00, 0x4a, 0x7e, 0xf5,
 0x73, 0x00, 0x00, 0x00, 0xd7, 0x49, 0x44, 0x41, 0x54, 0x38, 0xcb, 0x63, 0x60, 0x18, 0x60, 0x10,
 0xca, 0x1c, 0x6a, 0x19, 0x52, 0x15, 0xb2, 0x36, 0xe4, 0x4a, 0xc8, 0xbb, 0x90, 0x5f, 0x04, 0x14,
 0x07, 0xc9, 0x84, 0x74, 0x84, 0x3c, 0x09, 0xf9, 0x8f, 0x80, 0x78, 0x14, 0x47, 0x8a, 0x84, 0xcc,
 0x08, 0xf9, 0x09, 0x52, 0x14, 0x7c, 0x27, 0x64, 0x6a, 0x70, 0x64, 0xb0, 0x61, 0xa8, 0x50, 0x1a,
 0x2b, 0x4e, 0xe5, 0xc1, 0x91, 0x21, 0x6f, 0x80, 0x8a, 0xff, 0x04, 0x2f, 0x0d, 0xb1, 0x22, 0xe8,
 0xea, 0x34, 0xd6, 0xe0, 0x99, 0x20, 0x93, 0x43, 0x77, 0x84, 0xab, 0x11, 0xe1, 0x49, 0x5f, 0xae,
 0x90, 0x6d, 0x40, 0xe5, 0xdf, 0x42, 0x52, 0x88, 0x0a, 0x93, 0x34, 0x56, 0xb0, 0xf2, 0x97, 0xa1,
 0xc6, 0x44, 0x06, 0x62, 0xc8, 0x2c, 0xb0, 0x72, 0x75, 0x62, 0xc3, 0x3c, 0x0a, 0xe4, 0x18, 0xa2,
 0x4d, 0x07, 0x06, 0x24, 0x28, 0x64, 0x52, 0x88, 0x8e, 0x53, 0x60, 0xb8, 0x03, 0x43, 0x86, 0x68,
 0xe5, 0xc0, 0x58, 0xfd, 0x19, 0xf2, 0x87, 0xa8, 0x80, 0x84, 0x9a, 0xdf, 0x01, 0x8c, 0xd3, 0xa5,
 0x44, 0x2b, 0x6f, 0x60, 0x0a, 0x79, 0x0c, 0x74, 0x90, 0x25, 0xf1, 0x69, 0xd2, 0x12, 0xe8, 0xdd,
 0xdb, 0x0c, 0x8c, 0xc4, 0x6b, 0xa8, 0x06, 0x6a, 0x98, 0x42, 0x42, 0xaa, 0x07, 0xa6, 0xf7, 0xff,
 0xc1, 0x91, 0xa4, 0x68, 0xb8, 0x02, 0xf4, 0x81, 0x01, 0x29, 0x1a, 0xde, 0x86, 0xfc, 0x0f, 0x14,
 0x26, 0x45, 0x03, 0x30, 0xab, 0x84, 0xb2, 0x31, 0x0c, 0x12, 0x00, 0x00, 0x9b, 0x76, 0x5a, 0x51,
 0x53, 0x43, 0x76, 0x29, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE add_arc_xpm[1] = {{ png, sizeof( png ), "add_arc_xpm" }};

//EOF
