
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x02, 0x5d, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0xb5, 0x56, 0x4b, 0x6b, 0x13,
 0x51, 0x14, 0x8e, 0xa0, 0x5d, 0xe8, 0xff, 0x50, 0x94, 0xa2, 0xd6, 0xee, 0x25, 0x2a, 0x2a, 0xb6,
 0x46, 0xcd, 0x8b, 0x40, 0x42, 0xd4, 0x85, 0x76, 0x95, 0x9d, 0x90, 0x85, 0xbf, 0x20, 0x82, 0x60,
 0x62, 0x0b, 0x42, 0x4b, 0x7d, 0x40, 0x68, 0x97, 0xa9, 0x95, 0x18, 0xa5, 0x91, 0x99, 0x64, 0x16,
 0x71, 0x1c, 0x17, 0x1a, 0x90, 0x10, 0xa9, 0x53, 0x92, 0x20, 0xd4, 0x50, 0x8d, 0x88, 0x60, 0x9a,
 0x4c, 0x72, 0xbc, 0xe7, 0x36, 0x77, 0xc8, 0x64, 0x1e, 0xe9, 0x63, 0x7a, 0xe0, 0x83, 0x24, 0x67,
 0xee, 0xf7, 0x9d, 0xf3, 0xdd, 0x7b, 0x6e, 0xc6, 0xe1, 0xd0, 0xc6, 0x01, 0x97, 0xcb, 0x75, 0xd8,
 0xb1, 0x5f, 0xe1, 0xf5, 0x7a, 0xa7, 0x7d, 0x3e, 0xdf, 0x3f, 0x82, 0xfb, 0x4e, 0xa7, 0xf3, 0xa0,
 0xed, 0x02, 0x84, 0x38, 0x4b, 0x00, 0x3d, 0x48, 0x6e, 0xb7, 0x7b, 0x74, 0x5f, 0x04, 0x42, 0xa1,
 0x10, 0x13, 0x69, 0xda, 0xda, 0x0d, 0x13, 0x90, 0x24, 0x09, 0x92, 0xc9, 0x24, 0xf8, 0xfd, 0x7e,
 0x26, 0xf4, 0x99, 0xd8, 0x37, 0xb6, 0x5b, 0x52, 0xa1, 0xcf, 0x16, 0x8a, 0x62, 0xb1, 0x08, 0x18,
 0xa5, 0x52, 0x09, 0x22, 0x91, 0x08, 0xfb, 0xbd, 0x45, 0x10, 0x23, 0xa2, 0x23, 0xb6, 0x09, 0x60,
 0x34, 0x9b, 0x4d, 0x5b, 0xbb, 0xc9, 0x0e, 0x0a, 0xb0, 0xb0, 0xab, 0x1b, 0x55, 0x80, 0xe3, 0x38,
 0x4d, 0x57, 0x85, 0x42, 0x61, 0xef, 0xdd, 0x58, 0x75, 0xd0, 0x1f, 0xb2, 0x2c, 0x03, 0xcf, 0xf3,
 0x50, 0xad, 0x56, 0xa9, 0x68, 0xbb, 0xdd, 0x6e, 0x75, 0xbb, 0xdd, 0x2f, 0x24, 0x15, 0x27, 0x18,
 0xdd, 0xb3, 0xc0, 0xe6, 0x46, 0x03, 0xe4, 0x47, 0xf3, 0x20, 0x5d, 0xbf, 0x0b, 0xf9, 0x53, 0x57,
 0x28, 0xa4, 0x1b, 0x53, 0x20, 0xc7, 0xe7, 0x31, 0xa7, 0x90, 0x47, 0x66, 0x08, 0x46, 0x76, 0x25,
 0x50, 0xcf, 0xf0, 0x90, 0x1f, 0x9b, 0x04, 0xee, 0xe8, 0x39, 0x43, 0x60, 0xae, 0xfe, 0x86, 0x87,
 0x72, 0xb9, 0xfc, 0x27, 0x18, 0x0c, 0x2e, 0xed, 0x48, 0x00, 0xc9, 0xb9, 0x63, 0xe7, 0x0d, 0x89,
 0xf9, 0x13, 0x17, 0x61, 0x7d, 0x79, 0x05, 0xfe, 0x7e, 0x95, 0xe1, 0xfd, 0xa5, 0x30, 0x15, 0xc9,
 0x64, 0x32, 0xdd, 0xa1, 0x02, 0xb9, 0x5c, 0x4e, 0xb5, 0xc5, 0xac, 0x72, 0x24, 0xaf, 0xbf, 0xcd,
 0xab, 0x6b, 0x1a, 0xe2, 0x27, 0x10, 0xce, 0x4c, 0x42, 0xeb, 0x67, 0x43, 0x19, 0x2a, 0x80, 0xdf,
 0xe9, 0xc6, 0x12, 0xcf, 0xb7, 0x43, 0x8e, 0xb1, 0xfa, 0xe0, 0x09, 0xcd, 0xc9, 0xf1, 0xa7, 0x60,
 0x2a, 0x20, 0x08, 0x82, 0x7a, 0x44, 0x6b, 0xb5, 0x1a, 0x48, 0xd7, 0xee, 0xe8, 0xc9, 0x8f, 0x5f,
 0x80, 0x1f, 0xaf, 0xde, 0x69, 0xc8, 0xd7, 0x1e, 0x3f, 0x57, 0xf3, 0xb8, 0xf1, 0xdb, 0xee, 0x20,
 0x7f, 0x7a, 0x42, 0x47, 0xbe, 0xbe, 0x9c, 0xd5, 0x90, 0x57, 0x66, 0x17, 0x74, 0x1b, 0x3e, 0xf4,
 0xaa, 0x10, 0x45, 0x11, 0x3a, 0x9d, 0x8e, 0xce, 0xff, 0xef, 0x0b, 0x2f, 0xb5, 0x95, 0xcf, 0xbc,
 0xd0, 0x75, 0x88, 0xfb, 0x60, 0x29, 0xa0, 0x28, 0x0a, 0xa4, 0x52, 0xa9, 0x2d, 0x8b, 0xc8, 0x99,
 0x67, 0x0b, 0xb1, 0x52, 0xf1, 0xf2, 0x4d, 0xd8, 0xac, 0x6f, 0x6c, 0x55, 0x3e, 0xb7, 0x68, 0xb8,
 0x3f, 0x96, 0x16, 0x55, 0x2a, 0x15, 0x88, 0x46, 0xa3, 0x54, 0x30, 0x9d, 0x4e, 0xd3, 0x21, 0x62,
 0x0b, 0x99, 0x1d, 0x1f, 0x26, 0x6e, 0xc3, 0xb7, 0x87, 0xb3, 0xa6, 0x33, 0xb1, 0x96, 0x78, 0x66,
 0x2c, 0x10, 0x8b, 0xc5, 0x20, 0x10, 0x08, 0x50, 0x72, 0x72, 0xd7, 0xac, 0x92, 0xfb, 0xe7, 0x16,
 0x4e, 0x28, 0xb3, 0xc9, 0xcc, 0x73, 0x8d, 0x3d, 0xe3, 0x57, 0xa1, 0xf5, 0xeb, 0xb7, 0x62, 0xf5,
 0x97, 0xd9, 0x21, 0x48, 0xb0, 0x97, 0x00, 0x1c, 0x7f, 0x1c, 0x1e, 0xb3, 0x41, 0xd3, 0x80, 0x3c,
 0xd3, 0x3b, 0xba, 0x89, 0x41, 0x81, 0xd7, 0xac, 0x6a, 0x8f, 0xc7, 0x73, 0xb6, 0x3f, 0x87, 0x77,
 0x0b, 0x41, 0x16, 0x45, 0x70, 0xf3, 0xac, 0x2a, 0xef, 0x91, 0xaf, 0x10, 0x1c, 0x1a, 0x7c, 0xab,
 0x38, 0x49, 0x04, 0xee, 0x85, 0xc3, 0xe1, 0x23, 0x46, 0x97, 0x61, 0x4f, 0x64, 0x1a, 0x27, 0x14,
 0x87, 0xe8, 0xa3, 0x7b, 0x8a, 0x1e, 0x5f, 0x04, 0x7e, 0x46, 0xcf, 0xd1, 0x16, 0xac, 0x9c, 0x91,
 0xff, 0x07, 0xbd, 0x36, 0x09, 0x10, 0x55, 0xc7, 0x0c, 0x57, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45,
 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE libedit_xpm[1] = {{ png, sizeof( png ), "libedit_xpm" }};

//EOF
